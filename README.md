# wifi - How wireless device drivers are implemented in linux: 

A slightly detailed picture of how nl80211 and cfg80211 work with other parts of the system (user space, kernel, and hardware).
•	nl80211 is the interface between user space software (iw, wpa_supplicant, etc.) and the kernel (cfg80211 and mac80211 kernel modules, and specific drivers).
•	The WiFi drivers and hardware could be Full-MAC or Soft-MAC (see Wireless_network_interface_controller).
•	cfg80211_ops is a set of operations that Full-MAC drivers and mac80211 module register to cfg80211 module.
•	ieee80211_ops is a set of operations that Soft-MAC drivers register to mac80211 module

 

Cfg80211 must, directly or indirectly (via mac80211), be used by all modern wireless drivers in linux , so that they offer a consistent API through nl80211. 
	
An example of how wifi driver (e.g.: iwl) interfaces with mac80211:

static struct ieee80211_ops il3945_mac_ops __read_mostly = {
        .tx 		   = il3945_mac_tx,
        .start		   = il3945_mac_start,
        .stop 		   = il3945_mac_stop,
        .add_interface    = il_mac_add_interface,
        .remove_interface = il_mac_remove_interface,
        .change_interface = il_mac_change_interface,
        .config 	   = il_mac_config,
        .configure_filter = il3945_configure_filter,
        .set_key 	   = il3945_mac_set_key,
        .conf_tx	   = il_mac_conf_tx,
        .reset_tsf	   = il_mac_reset_tsf,
        .bss_info_changed = il_mac_bss_info_changed,
        .hw_scan 	   = il_mac_hw_scan,
        .sta_add	   = il3945_mac_sta_add,
        .sta_remove	   = il_mac_sta_remove,
        .tx_last_beacon   = il_mac_tx_last_beacon,
        .flush 		   = il_mac_flush,
};

•	ieee80211_ops maps driver implementation of the handlers to the common mac80211 API.
•	During driver registration, these handlers are registered to the mac80211 (through ieee80211_alloc_hw)

A high-level description of the Linux WiFi kernel stack:
1.	It's important to understand there are 2 paths in which userspace communicates with the kernel when we're talking about WiFi:
•	Data path: the data being received is passed from the wireless driver to the netdev core (usually using netif_rx()). From there the net core will pass it through the TCP/IP stack code and will queue it on the relevant sockets from which the userspace process will read it. On the Tx path packets will be sent from the netdev core to the wireless driver using the ndo_start_xmit() callback. The driver registers (like other netdevices such as an ethernet driver) a set of operations callbacks by using the struct net_device_ops.
•	Control path: This path is how userspace controls the WiFi interface/device and performs operations like scan / authentication / association. The userspace interface is based on netlink and called nl80211 (see include/uapi/linux/nl80211.h). You can send commands and get events in response.
2.	When you send an nl80211 command it gets initially handled by cfg80211 kernel module (it's code is under net/wireless and the handlers are in net/wireless/nl80211.c). cfg80211 will usually call a lower level driver. In case of Full MAC hardware the specific HW driver is right below cfg80211. The driver below cfg80211 registers a set of ops with cfg80211 by using cfg80211_ops struct. For example see brcmfmac driver (drivers/net/wireless/brcm80211/brcmfmac/wl_cfg80211.c)
3.	For Soft MAC hardware there's mac80211 which is a kernel module implementing the 802.11 MAC layer. In this case cfg80211 will talk to mac80211 which will in turn use the hardware specific lower level driver. An example of this is iwlwifi (For Intel chips).
4.	mac80211 registers itself with cfg80211 by using the cfg80211_ops (see net/mac80211/cfg.c). The specific HW driver registers itself with mac80211 by using the ieee80211_ops struct (for example drivers/net/wireless/iwlwifi/mvm/mac80211.c).
5.	Initialization of a new NIC you've connected occurs from the bottom up the stack. The HW specific driver will call mac80211's ieee80211_allow_hw() usually after probing the HW. ieee80211_alloc_hw() gets the size of private data struct used by the HW driver. It in turns calls cfg80211 wiphy_new() which does the actual allocation of space sufficient for the wiphy struct, the ieee80211_local struct (which is used by mac80211) and the HW driver private data (the layering is seen in ieee80211_alloc_hw code). ieee80211_hw is an embedded struct within ieee80211_local which is "visible" to the the HW driver. All of these (wiphy, ieee80211_local, ieee80211_hw) represent a single physical device connected.
6.	On top of a single physical device (also referred to as phy) you can set up multiple virtual interfaces. These are essentially what you know as wlan0 or wlan1 which you control with ifconfig. Each such virtual interface is represented by an ieee80211_vif. This struct also contains at the end private structs accessed by the HW driver. Multiple interfaces can be used to run something like a station on wlan0 and an AP on wlan1 (this is possible depending on the HW capabilities).


1) The net_device_ops structure is a kernel structure used to interface network device drivers to the kernel. It's an interface. The kernel knows about what's in the structure and knows how and when to call each function on a given device. 
2) The device driver for a particular hardware vendor also knows about this structure and populates it with functions to be called by the kernel. 
3) User-space operates on network devices via the socket interface and setting socket options.

Synopsis: (Vinod)
====================
In wi-fi case (softMAC implementation) , mac80211 module registers to cfg80211 via cfg80211_ops for configuration ,management frames etc . It also registers to device agnostic interface (netdevice framework -- low-level access to Linux network devices) using net_device_ops, to get packets to mac80211 module. The mac80211 module itself adds the wireless headers (subif_submit_start_xmit callback). Any wireless device driver which wants to take packets, must register to mac80211 module using ieee80211_ops. 


   

